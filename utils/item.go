package utils

import (
	"net/url"
	"regexp"
)

func MarketUrlToName(marketUrl string) string {
	/*
		(defn market-listing-url-to-hash-name [url]
		  (urldecode (second (re-find #"market/listings/[0-9]+/(.*)" url))))
		  :
	*/

	c, err := regexp.Compile("market/listings/[0-9]+/(.*)")

	if err != nil {
		panic(err)
	}

	match := c.FindStringSubmatch(marketUrl)

	if err != nil {
		panic(err)
	}

	if len(match) != 2 {
		panic("Match is not 2 long ([full, match])")
	}

	name, err := url.QueryUnescape(match[1])

	if err != nil {
		panic(err)
	}

	return name
}
