package database

import (
	_ "github.com/ziutek/mymysql/godrv"
	"github.com/ziutek/mymysql/mysql"
)

var Connection mysql.Conn

func Connect() {
	db := mysql.New("tcp", "", "localhost:3306", "root", "", "bookiegg")

	err := db.Connect()

	if err != nil {
		panic(err)
	}

	Connection = db
}
