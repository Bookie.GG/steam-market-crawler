package database

func StorePrice(itemId uint64, price float64, volume int) {
	_, _, err := Connection.Query("INSERT INTO csgo_item_prices "+
		"(`csgo_item_id`, `price`, `volume`, `created_at`, `updated_at`) "+
		"VALUES ('%d', '%f', '%d', NOW(), NOW())", itemId, price, volume)

	if err != nil {
		panic(err)
	}
}
