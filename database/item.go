package database

import "gitlab.com/Bookie.GG/marketcrawler/item_info"

func GetItemId(item item_info.Detail, exteriorId, qualityId uint64) uint64 {
	rows, _, err := Connection.Query("SELECT id FROM csgo_items "+
		"WHERE market_name = '%s' AND csgo_item_exterior_id = '%d' AND csgo_item_quality_id = '%d'", item.Name, exteriorId, qualityId)

	if err != nil {
		panic(err)
	}

	if len(rows) == 0 {
		stattrak := 0
		souvenir := 0

		if item.Souvenir {
			souvenir = 1
		}

		if item.StatTrak {
			stattrak = 1
		}

		_, res, err := Connection.Query("INSERT INTO csgo_items "+
			"(`csgo_item_exterior_id`, `csgo_item_quality_id`,`market_name`,`souvenir`,`stattrak`) "+
			"VALUES ('%d', '%d', '%s', '%d', '%d')", exteriorId, qualityId, item.Name, souvenir, stattrak)

		if err != nil {
			panic(err)
		}

		return res.InsertId()
	}

	return rows[0].Uint64(0)
}
