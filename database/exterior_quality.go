package database

func GetInfoItemId(tablename, exterior string) uint64 {
	rows, _, err := Connection.Query("SELECT id FROM %s WHERE name = '%s'", tablename, exterior)

	if err != nil {
		panic(err)
	}

	if len(rows) == 0 {
		_, res, err := Connection.Query("INSERT INTO %s (`name`, `created_at`, `updated_at`) VALUES ('%s', NOW(), NOW())", tablename, exterior)

		if err != nil {
			panic(err)
		}

		return res.InsertId()
	}

	return rows[0].Uint64(0)
}
