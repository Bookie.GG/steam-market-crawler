package item_info

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/fatih/color"

	"gitlab.com/Bookie.GG/marketcrawler/listing_url_crawler"
	"gitlab.com/Bookie.GG/marketcrawler/steam_http"
)

type Overview struct {
	MedianPrice float64 `json:"median_price"`
	LowestPrice float64 `json:"lowest_price"`
	Volume      int
	Success     bool
}

type Detail struct {
	Name     string
	Quality  string
	Exterior string
	Logo     string
	Overview Overview
	Souvenir bool
	StatTrak bool
}

var itemNameColor = color.New(color.FgYellow).Add(color.Bold).SprintFunc()
var failColor = color.New(color.FgRed).Add(color.Bold).PrintFunc()
var highlightColor = color.New(color.FgCyan).Add(color.Bold).SprintfFunc()

func Scrape(listingUrl listing_url_crawler.ListingUrl, currentIndex, total int) (d Detail, ok bool) {
	fmt.Printf("[%s/%s] Loading information about %s... ",
		color.CyanString("%04d", currentIndex+1),
		color.CyanString("%04d", total),
		itemNameColor(listingUrl.ItemName))

	r, ok := getOverview(listingUrl)

	var detail Detail

	if ok {
		printPrefix := "\n" + strings.Repeat(" ", 38) + "- "
		fmt.Printf(printPrefix+"%s items valuated at ~%s each",
			color.New(color.FgMagenta).Add(color.Bold).SprintfFunc()("%d", r.Volume),
			color.New(color.FgGreen).Add(color.Bold).SprintfFunc()("$%.2f", r.LowestPrice))

		detail, ok = getDetail(listingUrl)
		detail.Name = listingUrl.ItemName

		if ok {
			logoStatus := color.New(color.FgGreen).Add(color.Bold).SprintFunc()("✔")

			// 5 does not have any significance. Mostly just
			// empty logo has an x, non-empty has a tick
			// but the 5 takes in account various differences
			// such as newlines, weird bytes, ...
			if len(detail.Logo) < 5 {
				logoStatus = color.New(color.FgRed).Add(color.Bold).SprintFunc()("x")
			}

			fmt.Printf(printPrefix+"Exterior: %s, Quality: %s, Image: %s",
				highlightColor("%s", detail.Exterior),
				highlightColor("%s", detail.Quality),
				highlightColor("%s", logoStatus))

			detail.Overview = r
		} else {
			failColor(" Failed")
			return Detail{}, false
		}
	} else {
		failColor("Failed")
		return Detail{}, false
	}

	fmt.Println()

	return detail, true
}

func getDetail(listingUrl listing_url_crawler.ListingUrl) (Detail, bool) {
	response, err := steam_http.Get(listingUrl.Url)

	defer response.Body.Close()

	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(response.Body)
	_ = body

	if err != nil {
		panic(err)
	}

	ext := getExterior(listingUrl.ItemName)

	// extract data json to have
	// the data well escaped and then look for
	// quality and logos with regexp
	// this way of doing it will prevent false-positives
	// (someone having "type": "Fuck" in name and other issues)

	jsonString := extractByRegexp("var g_rgAssets = (.*);\r?\n", string(body))
	quality := extractByRegexp(",\"type\":\"([^\"]+)\"", jsonString)
	logo := extractByRegexp(",\"icon_url\":\"([^\"]+)\"", jsonString)

	isSouvenir := strings.Contains(listingUrl.ItemName, "Souvenir")
	isStatTrak := strings.Contains(listingUrl.ItemName, "StatTrak")

	return Detail{
		Exterior: ext,
		Quality:  quality,
		Logo:     logo,
		StatTrak: isStatTrak,
		Souvenir: isSouvenir,
	}, true
}

func extractByRegexp(r string, body string) string {
	c, err := regexp.Compile(r)

	if err != nil {
		panic(err)
	}
	matches := c.FindStringSubmatch(string(body))
	return matches[1]
}

/*

Not Painted
*/
var qualities = []string{
	"Field-Tested",
	"Minimal Wear",
	"Battle-Scarred",
	"Well-Worn",
	"Factory New",
	"other",
}

func getExterior(name string) string {
	for _, quality := range qualities {
		if strings.Contains(name, quality) {
			return quality
		}
	}

	return ""
}

func getOverview(listingUrl listing_url_crawler.ListingUrl) (Overview, bool) {
	overviewUrl := getSteamMarketOverviewUrl(listingUrl.ItemName)

	response, err := steam_http.Get(overviewUrl)

	defer response.Body.Close()

	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		panic(err)
	}

	var overviewPlain map[string]interface{}
	err = json.Unmarshal(body, &overviewPlain)

	if err != nil {
		panic(err)
	}

	if !containsAll(overviewPlain, []string{"lowest_price", "median_price", "volume", "success"}) {
		return Overview{}, false
	}

	return Overview{
		LowestPrice: dollarToFloat(overviewPlain["lowest_price"].(string)),
		MedianPrice: dollarToFloat(overviewPlain["median_price"].(string)),
		Volume:      stringToInt(overviewPlain["volume"].(string)),
		Success:     overviewPlain["success"].(bool),
	}, true
}

func containsAll(m map[string]interface{}, keys []string) bool {

	var ok bool

	for _, key := range keys {
		_, ok = m[key]
		if !ok {
			return false
		}
	}

	return true
}

func stringToInt(str string) int {
	str = strings.Replace(str, ",", "", -1)
	i, err := strconv.Atoi(str)

	if err != nil {
		panic(err)
	}

	return i
}

func dollarToFloat(dollar string) float64 {
	dollarRegexp, err := regexp.Compile("^&#36;([0-9]+.[0-9]+)")

	if err != nil {
		panic(err)
	}

	match := dollarRegexp.FindStringSubmatch(dollar)

	if len(match) != 2 {
		panic("Price is not in correct format!")
	}

	float, err := strconv.ParseFloat(match[1], 64)

	if err != nil {
		panic(err)
	}

	return float
}

func getSteamMarketOverviewUrl(market_name string) string {
	queryName := url.QueryEscape(market_name)

	return "http://steamcommunity.com/market/priceoverview/?currency=1&appid=730&market_hash_name=" + queryName
}

/*
(defn steam-market-overview-url [market-hash-name]
  (str "" market-hash-name))
*/
