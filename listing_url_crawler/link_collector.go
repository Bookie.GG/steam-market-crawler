package listing_url_crawler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"

	"github.com/fatih/color"

	"gitlab.com/Bookie.GG/marketcrawler/status_reporter"
	"gitlab.com/Bookie.GG/marketcrawler/steam_http"
	"gitlab.com/Bookie.GG/marketcrawler/utils"
)

type MarketResponse struct {
	Success     bool
	Start       int
	Pagesize    int
	TotalCount  int    `json:"total_count"`
	ResultsHtml string `json:"results_html"`
}

func Collect() []ListingUrl {
	var pageUrl string
	var collectedUrls []ListingUrl

	reporter := status_reporter.Create()

	reporter.CreateStage("loading", "Loading", color.New(color.FgMagenta).Add(color.Bold), "Loaded")
	reporter.CreateStage("parsing", "Parsing", color.New(color.FgWhite).Add(color.Bold), "Parsed")

	for i := 1; ; i++ {
		reporter.Begin("loading")

		pageUrl = getMarketPageUrl(i)

		if marketPage, ok := loadMarketPage(pageUrl); ok {
			reporter.Finish("loading")

			reporter.Begin("parsing")

			r, _ := regexp.Compile("https?://[^\"]+")

			matches := r.FindAllString(marketPage.ResultsHtml, -1)

			for _, element := range matches {
				if strings.Contains(element, "market/listings") {
					collectedUrls = append(
						collectedUrls,
						ListingUrl{
							Url:      element,
							ItemName: utils.MarketUrlToName(element),
						})
				}
			}

			reporter.Finish("parsing")
			reporter.Tick(marketPage.Start+marketPage.Pagesize, marketPage.TotalCount)
		} else {
			color.Red("Nothing to load")
			break
		}
	}

	return collectedUrls
}

func loadMarketPage(url string) (returnResponse *MarketResponse, ok bool) {
	response, err := steam_http.Get(url)

	if err != nil {
		return nil, false
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			return nil, false
		}

		marketResponse := &MarketResponse{}
		err = json.Unmarshal(contents, marketResponse)

		if err != nil {
			return nil, false
		}

		if !marketResponse.Success {
			return nil, false
		}

		if marketResponse.Start > marketResponse.TotalCount {
			return nil, false
		}

		return marketResponse, true
	}
}

func getMarketPageUrl(page int) string {
	return fmt.Sprintf("http://steamcommunity.com/market/search/render/"+
		"?query="+
		"&start=%d"+
		"&count=100"+
		"&search_descriptions=0"+
		"&sort_column=name"+
		"&sort_dir=asc"+
		"&appid=730"+
		"&category_730_ItemSet%%5B%%5D=any"+
		"&category_730_TournamentTeam%%5B%%5D=any"+
		"&category_730_Weapon%%5B%%5D=any", (page-1)*100)
}
