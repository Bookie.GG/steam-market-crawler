package listing_url_crawler

type ListingUrl struct {
	Url      string
	ItemName string
}

func GetAll() []ListingUrl {
	return Collect()
}
