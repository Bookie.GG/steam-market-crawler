package steam_http

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

func Get(url string) (*http.Response, error) {
	return safeGet(url, 0)
}

func safeGet(url string, depth int) (*http.Response, error) {
	resp, err := http.Get(url)

	if resp.StatusCode != 200 {
		if depth > 5 {
			fmt.Printf("Last code: %d\n", resp.StatusCode)
			fmt.Println("Steam HTTP can't load it in 5 time intervals")
			os.Exit(1)
		} else {
			time.Sleep(5 * time.Second)
			return safeGet(url, depth+1)
		}
	}

	return resp, err
}
