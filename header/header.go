package header

import (
	"fmt"

	"github.com/fatih/color"
)

func Print() {
	cyanPrint := color.New(color.FgCyan).Add(color.Bold).PrintlnFunc()
	fmt.Println()
	cyanPrint("  ___ _                    __  __          _       _      ___                 _                __   _ ")
	cyanPrint(" / __| |_ ___ __ _ _ __   |  \\/  |__ _ _ _| |_____| |_   / __|_ _ __ ___ __ _| |___ _ _  __ __/  \\ / |")
	cyanPrint(" \\__ \\  _/ -_) _` | '  \\  | |\\/| / _` | '_| / / -_)  _| | (__| '_/ _` \\ V  V / / -_) '_| \\ V / () || |")
	cyanPrint(" |___/\\__\\___\\__,_|_|_|_| |_|  |_\\__,_|_| |_\\_\\___|\\__|  \\___|_| \\__,_|\\_/\\_/|_\\___|_|    \\_/ \\__(_)_|")

	color.New(color.FgRed).Add(color.Bold).Println("  - Bookie.GG (c) 2015")

	fmt.Println()
}
