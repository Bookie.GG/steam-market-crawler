package status_reporter

import (
	"fmt"

	"github.com/fatih/color"
)

var successPrint = color.New(color.FgGreen).Add(color.Bold).PrintlnFunc()

type Stage struct {
	Begin  string
	Finish string
	Color  *color.Color
}

type StatusReporter struct {
	Current int
	Total   int
	Stages  map[string]Stage
}

func (sr *StatusReporter) CreateStage(key, begin string, color *color.Color, finish string) {
	sr.Stages[key] = Stage{Begin: begin, Finish: finish, Color: color}
}

func (sr *StatusReporter) Begin(key string) {
	text := sr.Stages[key].Begin
	if sr.Total > 0 {
		fmt.Printf("[%s/%s] %s ",
			color.CyanString("%04d", sr.Current),
			color.BlueString("%04d", sr.Total),
			sr.Stages[key].Color.SprintfFunc()("%s... ", text))
	} else {
		fmt.Printf("[%s/%s] %s ",
			color.CyanString("  ? "),
			color.BlueString(" ?  "),
			sr.Stages[key].Color.SprintfFunc()("%s... ", text))
	}
}

func (sr *StatusReporter) Finish(key string) {
	successPrint(sr.Stages[key].Finish)
}

func (sr *StatusReporter) Tick(current, total int) {
	sr.Current = current
	sr.Total = total
}

func Create() *StatusReporter {
	return &StatusReporter{
		Stages: make(map[string]Stage),
	}
}
