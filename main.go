package main

import (
	"fmt"
	"strings"

	"github.com/fatih/color"

	"gitlab.com/Bookie.GG/marketcrawler/database"
	"gitlab.com/Bookie.GG/marketcrawler/header"
	"gitlab.com/Bookie.GG/marketcrawler/item_info"
	"gitlab.com/Bookie.GG/marketcrawler/listing_url_crawler"
)

func main() {
	header.Print()

	color.Green("[Stage 1] Loading and parsing all Steam Market urls")
	itemUrls := listing_url_crawler.GetAll()

	color.Green("[Stage 2] Connecting to database")
	database.Connect()

	color.Green("[Stage 3] Downloading all item information")

	total := len(itemUrls)
	allItems := make([]item_info.Detail, 0)

	for index, itemUrl := range itemUrls {
		item, ok := item_info.Scrape(itemUrl, index, total)
		if ok {
			allItems = append(allItems, item)

			exteriorId := database.GetInfoItemId("csgo_item_exteriors", item.Exterior)
			qualityId := database.GetInfoItemId("csgo_item_qualities", item.Quality)

			itemId := database.GetItemId(item, exteriorId, qualityId)

			database.StorePrice(itemId, item.Overview.LowestPrice, item.Overview.Volume)

		} else {
			printPrefix := "\n" + strings.Repeat(" ", 38) + "- "
			fmt.Println(printPrefix + color.New(color.FgRed).Add(color.Bold).SprintFunc()("Couldn't scrape information correctly."))
		}
	}

}
